import { Box, Button, Typography } from "@mui/material";
import { mobileList } from "../views/Order";
import { useNavigate, useParams } from "react-router-dom";

const ProductDetailPage = () => {
    const navigate = useNavigate();

    const {id} = useParams();

    const product = mobileList.filter((e) => {
        return e.id == id
    })[0];

    const onBtnBackClicked = () => {
        navigate(-1, { replace: true })
    }

    return (
        <Box sx={{ border: "1px solid #c8c9cc", padding: "2rem", textAlign: "center" }}>
            <Typography variant="h5" component="div" style={{ marginBottom: "0.5rem" }}>
                {product.name}
            </Typography>

            <Box sx={{margin: "1rem auto"}}>
                <Typography variant="body2" style={{ marginBottom: "0.5rem" }}>
                    Price: {product.price} USD
                </Typography>
                <Typography variant="body2">
                    Quantity: {product.quantity}
                </Typography>
            </Box>

            <Button variant="contained" color="info" size="small" style={{ textTransform: "capitalize" }}
                onClick={onBtnBackClicked}
            >Back</Button>
        </Box>
    );
}

export default ProductDetailPage;