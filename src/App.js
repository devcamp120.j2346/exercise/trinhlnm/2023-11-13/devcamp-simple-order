import Body from "./views/Body";
import TitleComponent from "./views/TitleComponent";

function App() {
  return (
    <div style={{ padding: "1rem", margin: "0rem 2rem"}}>
      <TitleComponent/>
      <Body/>
    </div>
  );
}

export default App;
