import { Button, Card, CardActions, CardContent, Grid, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const OrderDetail = ({ name, price, quantity, buyClicked, id }) => {
    const navigate = useNavigate();

    const onBtnBuyClicked = () => {
        console.log(name);
        console.log(price);
        console.log(quantity);

        buyClicked(price);
    }

    const onBtnDetailClicked = () => {
        navigate("/detail/" + id);
    }

    return (
        <Grid item md={4}>
            <Card style={{ padding: "2rem", margin: "1rem" }}>
                <CardContent>
                    <Typography variant="h5" component="div" style={{ marginBottom: "0.5rem" }}>
                        {name}
                    </Typography>
                    <Typography variant="body2" style={{ marginBottom: "0.5rem" }}>
                        Price: {price} USD
                    </Typography>
                    <Typography variant="body2">
                        Quantity: {quantity}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button variant="contained" color="info" size="small" style={{ textTransform: "capitalize" }}
                        onClick={onBtnBuyClicked}
                    >Buy</Button>
                    <Button variant="contained" color="info" size="small" style={{ textTransform: "capitalize" }}
                        onClick={onBtnDetailClicked}
                    >Detail</Button>
                </CardActions>
            </Card>
        </Grid>
    );
}

export default OrderDetail;