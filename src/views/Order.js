import { Box, Grid } from "@mui/material";
import OrderDetail from "./OrderDetail";
import { useState } from "react";

export const mobileList = [
    {
        id: 1,
        name: "IPhone X",
        price: 900,
        quantity: 0
    },
    {
        id: 2,
        name: "Samsung S9",
        price: 800,
        quantity: 0
    },
    {
        id: 3,
        name: "Nokia 8",
        price: 650,
        quantity: 0
    }
];

const Order = () => {
    const [total, setTotal] = useState(0);

    const onBtnBuyClickedInOrder = (price) => {
        setTotal(total + price);
    }

    return (
        <Box sx={{border: "1px solid #c8c9cc", padding: "2rem"}}>
            <Grid container>
                {
                    mobileList.map((e, i) => {
                        return <OrderDetail 
                            name={e.name} price={e.price} quantity={e.quantity} id={e.id}
                            buyClicked = {onBtnBuyClickedInOrder}
                        ></OrderDetail>
                    })
                }
            </Grid>

            <h4 style={{fontWeight: "600", marginBottom: "0rem"}}>Total: {total} $</h4>
        </Box>
    );
}

export default Order;