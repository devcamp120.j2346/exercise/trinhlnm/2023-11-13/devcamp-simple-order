import NotFound from "./pages/NotFound";
import ProductDetailPage from "./pages/ProductDetailPage";
import SimpleOrderPage from "./pages/SimpleOrderPage";

const routes = [
    {path: "/", element: <SimpleOrderPage/>},
    {path: "/detail/:id", element: <ProductDetailPage/>},
    {path: "*", element: <NotFound/>}
];

export default routes;